- name: Development Hiring Actual vs Plan
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  definition: Employees are in the division "Engineering" and department is "Development".
  target: 283 by end of FY21
  org: Development Department
  health:
    level: 3
    reasons:
    - Development has currently slowed hiring to critical hires.  One person starting
      July 1st and two openings.
    - We have been able to keep up with attrition and now will be looking to grow the org.
  sisense_data:
    chart: 8637575
    dashboard: 504639
    embed: v2
- name: Sales Renewal CSAT
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Can we improve the sales renewal process to meet a 92% satisfaction
    rating from internal sales teams?
  target: Above 92%
  org: Development Department
  is_key: true
  health:
    level: 3
    reasons:
    - Currently tracking at above 92% for the financial quarter.
  sisense_data:
    chart: 9257321
    dashboard: 705845
    embed: v2
- name: Development Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-plan-vs-actuals"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Development Department
  is_key: true
  health:
    level: 3
    reasons:
    - We continue to spend less than planned
    - Based on new financial model provided by finance, we'll make adjustments
  urls:
  - https://app.periscopedata.com/app/gitlab/633242/Development-Non-Headcount-BvAs
- name: Development Average Location Factor
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: Below 0.54
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are above our target.
    - With hiring slowed, we can not effectively make headway on location factor except
      for critical roles.
  sisense_data:
    chart: 7066152
    dashboard: 504639
    embed: v2
- name: Development Overall Handbook Update Frequency Rate
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/development/**` over time. The calculation for the monthly overall handbook update frequency rate is the number of handbook updates divided by the number of team members in the Development Department for a given month.
  target: At or above 0.5
  org: Development Department
  is_key: true
  health:
    level: 2
    reasons:
    - In February, we saw an increase in the number of handbook frequency updates. Handbook update frequency rate has increased steadily since November. We are close to target.
  sisense_data:
    chart: 10466007
    dashboard: 621056
    shared_dashboard: 1224665d-3fd9-4f77-a1b2-46407e655407
    embed: v2
- name: Overall MRs
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: This shows the number of Merge Requests (MRs) on a month by month
    basis. We only consider MRs that contribute to our product. It’s important because it shows the overall development teams velocity
    and helps ensure that we are continually making our product better. The <a href="https://gitlab.com/clefelhocz1">VP
    of Development</a> is the DRI on <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/engineering_productivity_metrics_projects_to_include.csv">what
    projects are included</a>. Please note that not all MRs in GitLab will count towards
    monthly total.
  target: 20% increase quarter of quarter
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - We have recovered from dropping to a single code base and passed through 3000
      MRs a month.
  sisense_data:
    chart: 6204343
    dashboard: 463858
    embed: v2
  urls:
  - https://quality-dashboard.gitlap.com/groups/gitlab-org/monthly_merged_mrs
- name: CVE issue to update
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Measurement of time CVE being issued to our product being updated.
  target: 7 days (until further data is provided)
  org: Development Department
  health:
    level: 3
    reasons:
    - We are measuring in Sisense/Periscope and are currently under our targeted threshold
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/issues/5936
  sisense_data:
    chart: 7692993
    dashboard: 588449
    embed: v2
- name: Backend Unit Test Coverage
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: BE Unit Test coverage shows the unit test coverage of our code base.  As
    an example 95% represents that 95% of the LOC in our BE software is unit tested.  It’s
    important as it shows how much code is tested early in the development process.
  target: Above 95%
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - This metric's threshold for action is around 95%.
    - We could spend further time on coded tests, it’s not recommended at this point
      unless we are finding significant defect correlation in our non-unit-test coverage
      areas.
  sisense_data:
    chart: 6165137
    dashboard: 475029
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/475029/Source-Code-KPI's
- name: Frontend Unit Test Coverage
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: FE Unit Test coverage shows the unit test coverage of our code base.  As
    an example 95% represents that 95% of the LOC in our FE software is unit tested.  It’s
    important as it shows how much code is tested early in the development process.
  target: Above 75%
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are still in the process of moving our testing framework from Karma to Jest.
      Over 80% of the tests have been migrated, and we are on track to be fully migrated
      in Q2 FY21.
    - We are at a point where we can track Jest alone, and aim for our target there.
    - We're using the baseline that we had when we were using Karma completely as
      our target. Once we've established that as an accurate target, we'll aim to
      improve it.
  sisense_data:
    chart: 6165129
    dashboard: 475029
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/475029/Source-Code-KPI's
- name: Mean Time To Merge (MTTM)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: To be aligned with CycleTime from Development. Monthly Product MRs mean
    time to merge, it tells us on average how long it takes from initiating an MR
    to being merged. This metric includes only MRs authored only by team members in the Development Department. No community contributions are included. The <a href="https://gitlab.com/clefelhocz1">VP of Development</a>
    is the DRI on <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/engineering_productivity_metrics_projects_to_include.csv">what
    projects are included</a>.
  target: At or below 11 days
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - We are focused on increasing the maintainer pool to have additional reviewers
      in place.
    - Our goal is 14 days.
    - In September, the MTTM decreased by 1/3 of a day.
  sisense_data:
    chart: 9355335
    dashboard: 504639
    embed: v2
- name: Review To Merge Time (RTMT)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: To be aligned with review time from Development. Monthly Product MRs
    review to merge time (RTMT), it tells us on average how long it takes from requesting
    the first MR review to being merged. This metric includes only MRs authored only by team members in the Development Department. No community contributions are included. The <a href="https://gitlab.com/clefelhocz1">VP
    of Development</a> is the DRI on <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/engineering_productivity_metrics_projects_to_include.csv">what
    projects are included</a>.
  target: At or below 4.5 days
  org: Development Department
  is_key: true
  health:
    level: 2
    reasons:
    - We are focused on increasing the maintainer pool to have additional reviewers
      in place.
    - We decreased the target to 4.5 for January onwards. 
    - Slight decrease in February as team members come back from the holidays. February was close to target (4.66 vs 4.5 target). Maintainer ratio is an ongoing improvement to help with this.
  sisense_data:
    chart: 9461567
    dashboard: 504639
    embed: v2
- name: Open to Merge
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to measure the lifecycle of MRs and reduce the tail of MRs.  We
    don't expect to ever eliminate it because there can be unique cases, but we don't
    the tail trending up.
  target: "< 10% of MRs in the past 3 months are merged more than 14 days after they
    are opened"
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - We need to make sure the tail of the distribution is longer on the left hand
      side than on the right hand side and investigate where MRs are taking longer
      than 30 days to merge.
  sisense_data:
    chart: 9077390
    dashboard: 638729
    embed: v2
- name: Overall MRs by Type
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to measure the breakdown of our development investment by MR
    type/label. We only consider MRs that contribute to our product. If an MR has more than one of these labels, the highest one in the
    list takes precedence.
  target: "< 5% change in proportion of MRs with undefined label"
  org: Development Department
  is_key: false
  health:
    level: 0
    reasons:
    - In March, we saw an increase in the % of MRs with an undefined label
    - The Engineering Manager for each team is ultimately responsible for ensuring
      that these labels are set correctly.
  sisense_data:
    chart: 8261116
    dashboard: 504639
    embed: v2
- name: Say Do Ratios
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Say do ratios measure the number of product issues that were committed
    to a development phase vs. the number of product issues that were closed at the
    end of the release cycle. We don’t want to put pressure on teams to make commitments
    and push to deliver on those commitments. Rather, we want to optimize for <a href="/handbook/engineering/#velocity-over-predictability">velocity
    over predictability</a> to deliver more value per release. It’s important to note
    that this is also not used as a comparative metric across team but rather to help
    each team understand what they are consistently able to deliver during each release.
    Additionally, the say do ratios help us clean up issues and encourage conversations
    between engineering managers and product managers.
  target: Unknown
  org: Development Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/658030/Say-Do-Ratios
  health:
    level: 0
    reasons:
    - There is no defined target.
- name: Development Department Narrow MR Rate
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Development Department Narrow <a href="/handbook/engineering/merge-request-rate">MR
    Rate</a> is a performance indicator showing how many changes the Development Department
    implements directly in the GitLab product. The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts. This is the
    ratio of product MRs authored by team members in Development Department to the
    number of team members in the Development Department. It's important because it
    shows us how productivity within the Development Department has changed over time.
  target: Above 10 MRs per month
  org: Development Department
  is_key: true
  health:
    level: 3
    reasons:
    - In the past 6 months, the Development Department Narrow MR rate has stayed
      between 6.5 and 11, with smaller fluctuations month to month. From a department
      standpoint, our team members are performing just as effectively as they were
      a year ago.
    - In February, we reduced the target to 10 and are focusing on iteration, efficiency, scaling and quality.
    - January and February continue to increase. February is to comparable March - June 2020.
    - February had 18 work days (US holiday + Friends and Family company holiday). March has 22 work days (Friends and Family company holiday) and we expect the narrow MR rate to increase slightly.
  sisense_data:
    chart: 8934986
    dashboard: 686954
    embed: v2
- name: Largest Contentful Paint (LCP)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Largest Contentful Paint (LCP) is an important, user-centric metric for measuring the largest load speed visible on the web page. To provide a good user experience on GitLab.com, we strive to have the LCP occur within the first few seconds of the page starting to load. This LCP metric is reporting on our <a href="https://gitlab.com/ddevault/scdoc"> Projects Home Page</a>. LCP data comes from the <a href="https://gitlab.com/gitlab-data/analytics/tree/master/extract/graphite/"> Graphite database</a>. A <a href="https://dashboards.gitlab.net/d/performance-comparison/github-gitlab-performance?orgId=1">Grafana dashboard</a> is available to compare LCP of GitLab.com versus GitHub.com on key pages, in additon to a <a href="https://forgeperf.org/"> third party site</a> with a broader comparison.
  target: Below 2.5s at the 90th percentile
  org: Development Department
  is_key: true
  health:
    level: 3
    reasons:
    - This is a new KPI.
    - LCP p90 data is captured every 4 hours and we report on the latest value each day.
    - In February, only 5 out of 28 days went above target. We haven't been above target since February 10th.
  sisense_data:
    chart: 9998655
    dashboard: 761256
    embed: v2
- name: Contributions Outside Development
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to measure the number of contributors and contributions made outside of the Development Department. We
    do this by looking at the number of product MRs merged by members in the Development
    Department (Directors, EMs, ICs) vs the number of product MRs merged by members
    outside of the Development Department. It's important because it shows how much
    of what gets shipped is being done by members outside of the Development Department.
  target: Unknown
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - Nearly half of all product MRs that gets shipped is not coming from the Development
      Department
  sisense_data:
    chart: 8389074
    dashboard: 504639
    embed: v2
- name: Overall MRs to Maintainer Ratio
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We aim to keep the merge request per maintainer at a reasonable level.
  target: Below 20
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - The chart shows Frontend and DB maintainer load is roughly 2x of Backend, which
      means we need to incubate more DB and Frontend maintainers proactively.
    - Ratios have stabilized and gone down consistently.  We need to consider doing another sweep of possible addition to maintainers.
    - Load needs to be continued area of focus as we haven’t hit targets yet.
  sisense_data:
    chart: 8464415
    dashboard: 655064
    embed: v2
- name: Maintainers and Trainees
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: This tracks the number of maintainers and trainees over time.
  target: Unknown
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - Not enough trainees are converted into maintainers
  sisense_data:
    chart: 8842432
    dashboard: 475647
    embed: v2
- name: MR Rate Percentiles
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: The percentage of engineers who worked on less than X merge requests.
    Observing the MR rate distribution across individuals helps us understand how
    productivity distribution is changing over time.
  target: Unknown
  org: Development Department
  is_key: false
  health:
    level: 0
    reasons:
    - Due to slow hiring, we may see an increase in 25th percentile over time.
  sisense_data:
    chart: 8549646
    dashboard: 652520
    embed: v2
- name: Development Department New Hire Average Location Factor
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.54
  org: Development Department
  is_key: false
  health:
    level: 1
    reasons:
    - We've been consistently below our target for the past 6 months. With such a large department, we need to work
      with recruiting to drive this below our target.
  sisense_data:
    chart: 9389310
    dashboard: 719547
    embed: v2
- name: Average PTO per Development Team Member
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: This shows the average number of PTO days taken per Development Team Member. It is the ratio of PTO days taken to the number of team members in the Development Department each month. Looking at the average number of PTO days over time helps us understand increases or decreases in efficiency and ensure that team members are taking time off to keep a healthy work/life balance.
  target: TBD
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - Need to monitor on a monthly basis
  sisense_data:
    chart: 9603129
    dashboard: 730747
    embed: v2
