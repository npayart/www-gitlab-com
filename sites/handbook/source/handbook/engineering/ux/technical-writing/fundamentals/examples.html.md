---
layout: handbook-page-toc
title: "How to transform content into topic types"
description: "View examples of how to transform content into concept, task, reference, and troubleshooting topics."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

We often write topics that contain lots of disparate information.
We want to make sure we're telling the audience everything we know.

Reseach shows that information is easier to digest if it's presented in
repeatable patterns, so at GitLab we're moving toward using industry-standard
[topic types](https://docs.gitlab.com/ee/development/documentation/structure.html).

We want each topic to be a focused concept, task, reference, or
troubleshooting topic. This structure helps our users recognize patterns and
makes search more efficient.

The following examples are intended to help you understand how to take
existing information and move it into topics of specific types.

## Before

The following topic was trying to be all things to all people. It talked a bit about groups,
then spoke about where to find them. Finally, it reiterated what was visible in the UI.

![An example](example_1.png)

## After

The information is easier to scan if you move it into concepts and tasks.

### Concept

![An example](example_1_after_concept.png)

### Task

![An example](example_1_after_task.png)
